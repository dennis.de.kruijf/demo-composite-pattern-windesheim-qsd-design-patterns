using System;

namespace CompositePatternDemo
{
    public class File : FileComponent
    {
        public File(string name, int level)
        {
            Name = name;
            Level = level;
        }

        public override string Name { get; }
        public override int Level { get; }

        public override void Print()
        {
            // Create indentation tabs, based upon the given level.
            string tabs = new String('\t', Level);
            Console.WriteLine(tabs + Name);
        }
    }
}