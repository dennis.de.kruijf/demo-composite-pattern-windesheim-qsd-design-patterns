using System;

namespace CompositePatternDemo
{
    // Abstract class, contains methods for both the "Leaf" and "Node".
    public abstract class FileComponent
    {
        public virtual string Name
        {
            get
            {
                throw new NotImplementedException();
            }
        }
        
        
        public virtual int Level
        {
            get
            {
                throw new NotImplementedException();
            }
        }
        public virtual void Add(FileComponent menuComponent)
        {
            throw new NotImplementedException();
        }

        public virtual void Remove(FileComponent menuComponent)
        {
            throw new NotImplementedException();
        }
        public virtual FileComponent GetChild(int i)
        {
            throw new NotImplementedException();
        }    

        public virtual void Print()
        {
            throw new NotImplementedException();
        }

    }
}