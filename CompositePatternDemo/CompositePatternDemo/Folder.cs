using System;
using System.Collections.Generic;
using System.IO;

namespace CompositePatternDemo
{
    public class Folder : FileComponent
    {
        private readonly List<FileComponent> _fileComponents = new List<FileComponent>();

        public Folder(string name, int level)
        {
            Name = name;
            Level = level;
        }

        public override string Name { get; }

        public override int Level { get; }

        public override void Add(FileComponent fileComponent)
        {
            _fileComponents.Add(fileComponent);
        }

        public override void Remove(FileComponent fileComponent)
        {
            _fileComponents.Remove(fileComponent);
        }

        public override FileComponent GetChild(int i)
        {
            return _fileComponents[i];
        }

        public override void Print()
        {
            // Create indentation tabs, based upon the given level.
            string tabs = new String('\t', Level);
            const string UNDERLINE = "\x1B[4m";
            const string RESET = "\x1B[0m";
            // Write the directory name with underline.
            Console.WriteLine(tabs + UNDERLINE + Name + RESET);
            
            var iterator = _fileComponents.GetEnumerator();
            // Iterate through the children.
            while (iterator.MoveNext())
            {
                FileComponent fileComponent = iterator.Current;
                // Call the print method on the FileComponent. In case of a folder, this call is recursive. 
                fileComponent?.Print();
            }
        }
    }
}