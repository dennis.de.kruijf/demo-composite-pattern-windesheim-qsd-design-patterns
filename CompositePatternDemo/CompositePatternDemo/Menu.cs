using System.Collections.Generic;
using System.IO;

namespace CompositePatternDemo
{
    public class Menu
    {
        private string location;

        public FileComponent _rootDirectory;

        public Menu(string path)
        {
            location = path;
        }

        // Creates the rootDirectory and filling it recursively.
        public void PrintTree()
        {
            int level = 0;
            _rootDirectory = new Folder(location, level);
            LoadFiles(_rootDirectory, location, level);
            
            _rootDirectory.Print();
        }
        
        // Creates the rootDirectory and filling it non-recursively.
        public void PrintCurrentFolder()
        {
            _rootDirectory = new Folder(location, 0);
            LoadFiles(_rootDirectory, location);
            
            _rootDirectory.Print();
        }
        
        public void LoadFiles(FileComponent folder, string path, int level)
        {
            // Get all folders in path.
            List<string> folders = new List<string>(Directory.EnumerateDirectories(path));
            // Get all files in path.
            List<string> files = new List<string>(Directory.EnumerateFiles(path));

            foreach (var dir in folders)
            {
                Folder newFolder = new Folder(dir.Substring(dir.LastIndexOf(Path.DirectorySeparatorChar) + 1), level);
                folder.Add(newFolder);
                // Recursive call to search in subdirectories.
                LoadFiles(newFolder, dir, level + 1);
            }

            foreach (var file in files)
            {
                folder.Add(new File(file.Substring(file.LastIndexOf(Path.DirectorySeparatorChar) + 1), level));
            }
        }
        
        public void LoadFiles(FileComponent folder, string path)
        {
            // Get all folders in path.
            List<string> folders = new List<string>(Directory.EnumerateDirectories(path));
            // Get all files in path.
            List<string> files = new List<string>(Directory.EnumerateFiles(path));

            foreach (var dir in folders)
            {
                Folder newFolder = new Folder(dir.Substring(dir.LastIndexOf(Path.DirectorySeparatorChar) + 1), 0);
                // Add new folder to current folder.
                folder.Add(newFolder);
            }

            foreach (var file in files)
            {
                // Add new file to current folder.
                folder.Add(new File(file.Substring(file.LastIndexOf(Path.DirectorySeparatorChar) + 1), 0));
            }
        }
        
    }
}