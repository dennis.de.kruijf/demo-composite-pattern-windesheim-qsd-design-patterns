﻿using System;
using System.IO;

namespace CompositePatternDemo
{
    public class Program
    {
        public static void CreateFile(string path, string filename)
        {
            using (StreamWriter outputFile = new StreamWriter(Path.Combine(path, filename)))
            {
                outputFile.WriteLine("Some text in some file.");
            } 
        }
        public static string SetUp()
        {
            // Mandatory setup to run project.
            var tempDirectory = Path.Combine(System.IO.Path.GetTempPath(), "CompositePatternDemo");
            if (!Directory.Exists(tempDirectory))
            {
                Console.WriteLine("Creating temp directory for project...");
                Directory.CreateDirectory(tempDirectory);
            }

            if (Directory.GetDirectories(tempDirectory).Length == 0)
            {
                var mainDirectory = Path.Combine(tempDirectory, "MainFolder");
                var currentFolder = mainDirectory;
                
                Directory.CreateDirectory(currentFolder);
                CreateFile(currentFolder, "File1.txt");

                currentFolder = Path.Combine(mainDirectory, "SubFolder1");
                Directory.CreateDirectory(currentFolder);
                CreateFile(currentFolder, "File_1_a.txt");
                CreateFile(currentFolder, "File_1_b.txt");
                CreateFile(currentFolder, "File_1_c.txt");
                
                currentFolder = Path.Combine(mainDirectory, "SubFolder2");
                Directory.CreateDirectory(currentFolder);
                CreateFile(currentFolder, "File_2_a.txt");
                
                currentFolder = Path.Combine(currentFolder, "SubSubFolder2a");
                Directory.CreateDirectory(currentFolder);
                CreateFile(currentFolder, "File_2_a_1.txt");
                CreateFile(currentFolder, "File_2_a_2.txt");
                
                currentFolder = Path.Combine(mainDirectory, "SubFolder3");
                Directory.CreateDirectory(currentFolder);
                CreateFile(currentFolder, "File_3_a.txt");
                CreateFile(currentFolder, "File_3_b.txt");
            }

            Console.WriteLine("Using directory: " + tempDirectory);
            return tempDirectory;
        }
        public static void Main(string[] args)
        {
            // Write options to console.
            Console.WriteLine("Composite pattern demo");
            Console.WriteLine("Choose a demo:");
            Console.WriteLine("1) Files and folders (current level)");
            Console.WriteLine("2) Files and folders (whole tree)");
            Console.WriteLine("--------------------------");
            
            // Using setup temp path, alternatively enter your own absolute path.
            string path = SetUp();

            // Create the menu based on the given path.
            Menu menu = new Menu(path);

            // Wait for key to be entered.
            ConsoleKeyInfo option = Console.ReadKey();
            Console.Clear();

            switch (option.Key)
            {
                // If options 1 is chosen.
                case ConsoleKey.D1:
                    // Print current folder, no tree.
                    menu.PrintCurrentFolder();
                    break;
                // If options 2 is chosen.
                case ConsoleKey.D2:
                    // Print full tree from current folder.
                    menu.PrintTree();
                    break;
            }
            
            
        }
    }
}