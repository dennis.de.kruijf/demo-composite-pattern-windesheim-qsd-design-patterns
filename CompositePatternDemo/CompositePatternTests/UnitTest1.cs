using CompositePatternDemo;
using Microsoft.VisualStudio.TestPlatform.TestHost;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CompositePatternTests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void checkifRootDirectoryIsSet()
        {
            string path = CompositePatternDemo.Program.SetUp();

            // Create the menu based on the given path.
            Menu menu = new Menu(path);
            
            // Print current folder to make sure the correct files are loaded.
            menu.PrintCurrentFolder();
            
            // Assert if the rootDirectory is set in our tree.
            Assert.IsInstanceOfType(menu._rootDirectory, typeof(Folder));
        }
        
        [TestMethod]
        public void checkIfFileIsFound()
        {
            string path = CompositePatternDemo.Program.SetUp();

            // Create the menu based on the given path.
            Menu menu = new Menu(path);
            
            // Print current folder to make sure the correct files are loaded.
            menu.PrintCurrentFolder();

            // Get the child object, in this case 'test.txt'.
            var child = menu._rootDirectory.GetChild(1);
            
            // Assert if the child is a File object in our tree.
            Assert.IsInstanceOfType(child, typeof(File));
        }
        
        [TestMethod]
        public void checkRecursiveFolders()
        {
            string path = CompositePatternDemo.Program.SetUp();

            // Create the menu based on the given path.
            Menu menu = new Menu(path);
            
            // Print the whole tree, to see if the recursive directories are set.
            menu.PrintTree();

            // Get the child object, in this case 'Folder 2'.
            var child = menu._rootDirectory.GetChild(0);
            
            // Get the child object, in this case 'Folder 4'.
            var subchild = child.GetChild(0);

            // Assert if the subchild is a Folder object in our tree.
            Assert.IsInstanceOfType(subchild, typeof(Folder));
        }
        
        [TestMethod]
        public void checkRecursiveFoldersAndFile()
        {
            string path = CompositePatternDemo.Program.SetUp();

            // Create the menu based on the given path.
            Menu menu = new Menu(path);
            
            // Print the whole tree, to see if the recursive directories are set.
            menu.PrintTree();

            // Get the child object, in this case 'Folder 2'.
            var child = menu._rootDirectory.GetChild(0);
            
            // Get the child object, in this case 'Folder 4'.
            var subchild = child.GetChild(0);

            // Get the child object, in this case 'test4.txt'.
            var subchildFile = subchild.GetChild(0);

            // Assert if the subchildFile is a File object in our tree.
            Assert.IsInstanceOfType(subchildFile, typeof(File));
        }
    }
}